unit SimpleLogger;

//  MIT License
//
//  Copyright (c) 2020 Sergey Kozubenko (SeaJay)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

//FIXME
interface
uses
  sysUtils;
type
  TSimpleLogger = Class(TObject)
  private
    toStdout : boolean;
    debugEnabled : boolean;
    aAddTime : boolean;
    fileName : String;
    procedure toLog(msg: string);
  public
    constructor Create(logPath : String);
    procedure debug(msg : String);
    procedure info(msg : String);
    procedure warn(msg : String);
    procedure error(msg : String);

    procedure debugEnable;
    procedure debugDisable;

    procedure stdoutEnable;
    procedure stdoutDisable;
  end;

implementation

constructor TSimpleLogger.Create(logPath : String);
var
  dateString : String;
begin
  aAddTime := true;
  
  DateTimeToString(dateString, 'dd-mm-yyyy_hh-mm-ss', Now);
  CreateDir(logPath);
  self.fileName := logPath + dateString +'.log';
  self.debugEnable;
  self.stdoutDisable;
end;


procedure TSimpleLogger.debug(msg : String);
begin
  if (self.debugEnabled) then self.toLog('[DEBUG]: ' + msg);
end;

procedure TSimpleLogger.info(msg : String);
begin
  self.toLog('[INFO]: ' + msg);
end;

procedure TSimpleLogger.warn(msg : String);
begin
  self.toLog('[WARN]: ' + msg);
end;

procedure TSimpleLogger.error(msg : String);
begin
  self.toLog('[ERROR]: ' + msg);
end;

procedure TSimpleLogger.debugEnable;
begin
  self.debugEnabled := true;
end;

procedure TSimpleLogger.debugDisable;
begin
  self.debugEnabled := false;
end;

procedure TSimpleLogger.stdoutEnable;
begin
  self.toStdout := true;
end;

procedure TSimpleLogger.stdoutDisable;
begin
  self.toStdout := false;
end;

procedure TSimpleLogger.toLog(msg : string);
var
  lm  : string;
begin
  if (self.aAddTime) then
    DateTimeToString(lm, '[dd.mm.yyyy] [hh:mm:ss]: ', Now)
  else
    lm := '';
  writeln(lm + msg);
end;
end.
