unit SlowStringMap;

//  MIT License
//
//  Copyright (c) 2020 Sergey Kozubenko (SeaJay)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

interface
uses
  DB, ADODB, Forms, classes, SysUtils;
type
  StringArray = array of String;
  TSlowStringMap = Class(TObject)
  private
    StringList : TStringList;
    protected
  public
    constructor Create();

    procedure put(key, value : String);
    function get(key : String) : String;
    
    function keys() : TStringList;
    function values() : TStringList;
    function Count() : Integer;
  published
  end;
implementation

constructor TSlowStringMap.Create();
begin
  self.StringList := TStringList.Create;
end;

procedure TSlowStringMap.put(key, value : String);
begin
  self.StringList.Values[key] := value;
end;

function TSlowStringMap.get(key : String) : String;
begin
  get := self.StringList.Values[key];
end;

function TSlowStringMap.keys() : TStringList;
var
  t : TStringList;
  i : longword;
begin
  t := TStringList.Create;
  for i := 0 to self.StringList.Count - 1 do begin
    t.Add(self.StringList.Names[i]);
  end;
  keys := t;
end;

function TSlowStringMap.values() : TStringList;
var
  t : TStringList;
  i : longword;
begin
  t := TStringList.Create;
  for i := 0 to self.StringList.Count - 1 do begin
    t.Add(self.StringList.ValueFromIndex[i]);
  end;
  values := t;
end;

function TSlowStringMap.Count() : Integer;
begin
  Count := self.StringList.Count;
end;
end.
